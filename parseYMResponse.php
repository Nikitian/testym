<?php

include_once 'checkYandexPurse.php';

function parseYMResponse(string $response): ?array
{
    $result = [
        'purse' => null,
        'password' => null,
        'amount' => null,
    ];
    preg_match_all('|\d{11,16}|', $response, $matchesPurse);
    if (count($matchesPurse[0]) > 0) {
        foreach ($matchesPurse[0] as $testPurse) {
            if (checkYandexPurse($testPurse)) {
                $result['purse'] = $testPurse;
                $response = str_replace($result['purse'], '', $response);
                break;
            }
        }
    }

    if ($result['purse'] === null) {
        return null;
    }

    preg_match('|(\d+[\.,]*\d{0,2})\s?' . preg_quote('р') . '|U', $response, $matchesAmount);
    if (isset($matchesAmount[1])) {
        $result['amount'] = (float)str_replace(',', '.', $matchesAmount[1]);
        $response = str_replace($matchesAmount[1], '', $response);
    } else {
        return null;
    }

    preg_match('|\d+|', $response, $matchesPassword);
    if (isset($matchesPassword[0])) {
        $result['password'] = $matchesPassword[0];
        return $result;
    }

    return null;
}
